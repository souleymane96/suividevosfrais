package fr.yamishadow.gsbandroid.modele;

import android.util.Log;
import org.json.JSONArray;

import fr.yamishadow.gsbandroid.Controle;
import fr.yamishadow.gsbandroid.outils.AccesHTTP;
import fr.yamishadow.gsbandroid.outils.AsyncResponse;

/**
 * Created by JEAN on 21/11/2016.
 */

public class AccessDistant implements AsyncResponse {

    String SERVERADDR = "http://192.168.0.17/ppegsb1/gsbAndroid/serveurgsbandroid.php";
    private Controle controle;

    public AccessDistant(){
        controle = Controle.getInstance(null);
    }

    @Override
    public void processFinish(String output) {

        String[] message = output.split("%");

        switch(message[0]){

            case "correct" :
                controle.envoiFrais(message[1]);
                break;
        }

    }

    /**
     * methode qui demande le profil à la page php
     * @param operation
     * @param lesDonneesJSON
     */
    public void envoi(String operation, JSONArray lesDonneesJSON, JSONArray idVisiteur){
        AccesHTTP accesDonnees = new AccesHTTP();
        accesDonnees.delegate = this;
        accesDonnees.addParam("operation", operation);
        accesDonnees.addParam("lesDonnees", lesDonneesJSON.toString());
        accesDonnees.execute(SERVERADDR);
    }
}
