package fr.yamishadow.gsbandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class IdentificationActivity extends AppCompatActivity {

    private EditText login;
    private EditText mdp;
    Controle controle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identification);
        controle = Controle.getInstance(this);
        valider();
        init();
    }

    private void valider(){

        ((Button)findViewById(R.id.btn_valide)).setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
               String visiteur = login.getText().toString();
               String password = mdp.getText().toString();
                controle.authentification(visiteur, password);
            }
        }) ;

    }

    private void init(){
        login =((EditText)findViewById(R.id.login));
        mdp = ((EditText)findViewById(R.id.mdp));
    }

}
