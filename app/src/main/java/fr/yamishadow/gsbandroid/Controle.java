package fr.yamishadow.gsbandroid;

import android.content.Context;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import fr.yamishadow.gsbandroid.modele.AccessDistant;

/**
 * Created by soule on 13/04/2017.
 */

public class Controle {

    private static Controle instance = null;
    private static Context contexte;
    private static AccessDistant accesDistant;

    /**
     *
     * @return
     */
    public final static Controle getInstance(Context uncontext){

        if (Controle.instance == null){

            Controle.instance = new Controle() ;
            accesDistant = new AccessDistant() ;
            Controle.contexte = uncontext;
        }
        return  Controle.instance;
    }

    private Controle(){
        super();
    }

    public void authentification(String login, String mdp){

        List authentification = new ArrayList();
        authentification.add(login);
        authentification.add(mdp);
        accesDistant.envoi("connexion", convertoJsonArray(authentification),null);
    }

    public JSONArray convertoJsonArray(List laListe){
        return (new JSONArray(laListe));
    }

    public void envoiFrais(String id){

        List listIdVisiteur = new ArrayList();
        listIdVisiteur.add(id);
        Enumeration enumeration = Global.listFraisMois.keys();

        while(enumeration.hasMoreElements()){

            //accesDistant.envoi("envoiFrais",convertoJsonArray(Global.listFraisMois.get(enumeration.nextElement())));
        }

    }

}
